from random import randint
import secrets
from urllib import response


def generate_secret():
    rand_list = []
    while len(rand_list) < 4:
        n = randint(0,9)
        if n not in rand_list:
            rand_list.append(n)
    print("* * * *")
    return rand_list
       

def prompt_call(guess):
    guess_list = []
    for guess_temp in guess:
        guessint = int(guess_temp)
        guess_list.append(guessint)
        print("Guess List: ", guess_list )
    return guess_list

def exact_matched(a, b):
    count = 0
    for a, b in zip(a, b):
        if a == b:
            count += 1
    return count      

def count_number_of_common(a, b):
    common_entries = 0
    for match in a:
        if match in b:
            common_entries += 1
    return common_entries    



def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        while len(guess) != 4:
            print("Please enter a four-digit number! ")
            guess = input(prompt)

        converted_guess = prompt_call(guess)

        total_exact_matches = exact_matched(converted_guess, secret)

        total_count_common = count_number_of_common(converted_guess, secret)

        if total_exact_matches == 4:
            print("You have won!")
            return

        print("Total number of bulls: ", total_exact_matches)
       
        print("Total number of cows: ", total_count_common - total_exact_matches)

    print(secret)


def run():
    response = input("Do you wanna play a game of Bulls and Cows?" "Y or N: ")
    if response == "Y":
        play_game()
    elif response == "N":
        exit()
    else:
        print("I'm sorry I didn't understand that, can you try typing Y or N again? ")


if __name__ == "__main__":
    run()
